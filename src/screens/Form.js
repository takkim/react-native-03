import React, { useState, useEffect, useRef } from "react";
import styled from "styled-components/native";

const Container = styled.View`
  flex: 1;
  background-color: #fff;
  align-items: center;
  justify-content: center;
`;

const Text = styled.Text`
  font-size: 24px;
`;

const Button = styled.Button``;

const Input = styled.TextInput`
  border: 1px solid black;
  width: 80%;
  height: 50px;
`;

const Form = ({ navigation, route }) => {
  const [name, setName] = useState("");
  const [email, setemail] = useState("");
  const nameInput = useRef();

  useEffect(() => {
    console.log(name, email);
  }, [email, name]);

  useEffect(() => {
    console.log("mounted!!!");
    nameInput.current.focus();
    // 언마운트 되는 시점 => return 함수!
    return () => {
      console.log("unmounted!!");
    };
  }, []);

  return (
    <>
      <Text>{name}</Text>
      <Text>params : {route.params.name}</Text>
      <Input value={name} onChangeText={setName} ref={nameInput} />
      <Text>{email}</Text>
      <Input value={email} onChangeText={setemail} />
      <Button title="이전으로" onPress={() => navigation.goBack()} />
    </>
  );
};

export default Form;
