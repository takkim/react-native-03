import React, { useState } from "react";
import styled from "styled-components/native";

const Container = styled.View`
  flex: 1;
  background-color: #fff;
  align-items: center;
  justify-content: center;
`;

const Text = styled.Text`
  font-size: 24px;
`;

const Button = styled.Button``;

export default function App() {
  // state - counter (useState)
  // 구조 분해 할당 (Desturcturing assignment)
  const [counter, setCounter] = useState(0);
  // const counterState = useState(0);
  // const counter = counterState[0]
  // const setCounter = counterStaete[1]

  return (
    <Container>
      <Text>{counter}</Text>
      {/* 버튼을 클릭하면, 숫자가 증가 */}
      <Button
        title="클릭!"
        onPress={() => {
          // setCounter(counter + 1);
          // setCounter(counter + 1);
          setCounter((counter) => counter + 1);
          setCounter((counter) => counter + 1);
        }}
      />
    </Container>
  );
}
