import React, { useState, useEffect } from "react";
import styled from "styled-components/native";
import Form from "./components/Form";

const Container = styled.View`
  flex: 1;
  background-color: #fff;
  align-items: center;
  justify-content: center;
`;

const Button = styled.Button``;

export default function App() {
  const [isEnabled, setIsEnabled] = useState(false);
  return (
    <Container>
      <Button title="toggle" onPress={() => setIsEnabled((state) => !state)} />
      {isEnabled && <Form />}
    </Container>
  );
}
