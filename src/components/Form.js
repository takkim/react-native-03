import React, { useState, useEffect, useRef } from "react";
import styled from "styled-components/native";

const Container = styled.View`
  flex: 1;
  background-color: #fff;
  align-items: center;
  justify-content: center;
`;

const Text = styled.Text`
  font-size: 24px;
`;

const Button = styled.Button``;

const Input = styled.TextInput`
  border: 1px solid black;
  width: 80%;
  height: 50px;
`;

const Form = () => {
  const [name, setName] = useState("");
  const [email, setemail] = useState("");
  const nameInput = useRef();
  // 이메일 값이 변경되면, 그때마다 이름과 이메일을 모두 출력!
  // useEffect(() => {
  //   console.log(name, email);
  // }, [email]);
  useEffect(() => {
    console.log(name, email);
  }, [email, name]);

  // rr <- 새로고침
  // 마운트 되는 시점!
  useEffect(() => {
    console.log("mounted!!!");
    nameInput.current.focus();
    // 언마운트 되는 시점 => return 함수!
    return () => {
      console.log("unmounted!!");
    };
  }, []);

  return (
    <>
      <Text>{name}</Text>
      <Input value={name} onChangeText={setName} ref={nameInput} />
      <Text>{email}</Text>
      <Input value={email} onChangeText={setemail} />
    </>
  );
};

export default Form;
