import React from "react";
import styled from "styled-components/native";

// styled-components
const Row = styled.View`
  width: 100%;
  flex-direction: row;
  justify-content: space-between;
  padding: 5px 20px;
`;

const Text = styled.Text`
  font-size: 18px;
  font-weight: bold;
`;

// 컴포넌트
const Coin = ({ name, price_usd }) => {
  // console.log(props);
  return (
    <Row>
      <Text>{name}</Text>
      <Text>${price_usd}</Text>
    </Row>
  );
};

export default Coin;
