import React, { useContext, useState } from "react";
import styled from "styled-components/native";
import TodoContext from "../contexts/Todo";

const Text = styled.Text`
  font-size: 24px;
`;

const Input = styled.TextInput`
  border: 1px solid black;
  width: 80%;
  height: 50px;
`;

const Todo = () => {
  const [userInput, setUserInput] = useState("");
  const { content, setContent } = useContext(TodoContext);
  return (
    <>
      <Text>{content}</Text>
      <Input
        value={userInput}
        onChangeText={setUserInput}
        onSubmitEditing={() => {
          setContent(userInput);
          setUserInput("");
        }}
      />
    </>
  );
};

export default Todo;
