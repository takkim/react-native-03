import React, { createContext, useState } from "react";

const TodoContext = createContext({
  content: "기본 Todo",
  setContent: () => {},
});

// <TodoContext.Provider></TodoContext.Provider>
const TodoProvider = ({ children }) => {
  const [content, setContent] = useState("");
  const value = { content, setContent };
  return <TodoContext.Provider value={value}>{children}</TodoContext.Provider>;
};

const TodoConsumer = TodoContext.Consumer;

export { TodoProvider, TodoConsumer };
export default TodoContext;
