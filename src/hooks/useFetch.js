// 데이터를 가지고 오는 로직
import { useState, useEffect } from "react";
import axios from "axios";

export const useFetch = (url) => {
  const [data, setData] = useState([]);
  // mount시 axios 요청
  useEffect(() => {
    axios.get(url).then((response) => {
      console.log(response.data.data);
      // 요청 성공하면, data 값 반영
      setData(response.data.data);
    });
    // mount시 => []
  }, []);

  return [data];
};
// 데이터 반환
