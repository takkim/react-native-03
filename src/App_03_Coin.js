import React from "react";
import styled from "styled-components/native";
import Coin from "./components/Coin";
import { useFetch } from "./hooks/useFetch";

const Container = styled.View`
  flex: 1;
  background-color: #fff;
  align-items: center;
  justify-content: center;
`;

export default function App() {
  const [data] = useFetch("https://api.coinlore.net/api/tickers/?limit=10");
  return (
    <Container>
      {data.map(({ symbol, name, price_usd }) => {
        return <Coin key={symbol} name={name} price_usd={price_usd} />;
      })}
    </Container>
  );
}
